#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <string>

using namespace std;

class Carrinho{
private:
  //Attributes
   string pagamento;

public:
  //builders
  Carrinho();
  Carrinho(string pagamento);
  //destructor
  ~Carrinho();
  //set
  void set_pagamento(string pagamento);
  //get
  string get_pagamento();
  //methods
};
#endif
