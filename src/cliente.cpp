#include "cliente.hpp"
#include <iostream>
//--------------------------Builders--------------------------------------------
Cliente::Cliente(){
  nome = "Generico";
  cpf = "000.000.000-00";
  naturalidade = "Generico";
  endereco = "Generico";
  profissao = "Generico";
  idade = 0;
}

Cliente::Cliente(string nome, string cpf, string naturalidade, string endereco, string profissao, int idade){
  set_nome(nome);
  set_cpf(cpf);
  set_naturalidade(naturalidade);
  set_endereco(endereco);
  set_profissao(profissao);
  set_idade(idade);
}

//--------------Destructor------------------------------------------------------
Cliente::~Cliente(){
  cout << "Destrutor do objeto" << endl;
}
//-----------------SET----------------------------------------------------------
void Cliente::set_nome(string nome){
  this->nome = nome;
}
void Cliente::set_cpf(string cpf){
  this->cpf = cpf;
}
void Cliente::set_naturalidade(string naturalidade){
  this->naturalidade = naturalidade;
}
void Cliente::set_endereco(string endereco){
  this->endereco = endereco;
}
void Cliente::set_profissao(string profissao){
  this->profissao = profissao;
}
void Cliente::set_idade(int idade){
  this->idade = idade;
}
//-----------------GET----------------------------------------------------------
string Cliente::get_nome(){
  return nome;
}
string Cliente::get_cpf(){
  return cpf;
}
string Cliente::get_naturalidade(){
  return naturalidade;
}
string Cliente::get_endereco(){
  return endereco;
}
string Cliente::get_profissao(){
  return profissao;
}
int Cliente::get_idade(){
  return idade;
}
