#include "carrinho.hpp"
#include <iostream>
//--------------------------Builders--------------------------------------------
Carrinho::Carrinho(){
  pagamento = "Nenhum";
}
Carrinho::Carrinho(string pagamento){
  set_pagamento(pagamento);
}
//--------------Destructor------------------------------------------------------
Carrinho::~Carrinho(){
  cout << "Destrutor do objeto" << endl;
}
//-----------------SET----------------------------------------------------------
void Carrinho::set_pagamento(string pagamento){
  this->pagamento = pagamento;
}
//-----------------GET----------------------------------------------------------
string Carrinho::get_pagamento(){
  return pagamento;
}
//methods
