#include "produtos.hpp"
#include <iostream>

//--------------------------Builders--------------------------------------------
Produtos::Produtos(){
 quantidade = 0;
 codigo = 0;
 editora ="Generico";
 titulo = "Generico";
 preco = 0.0;
}
Produtos::Produtos(int quantidade, int codigo, float preco){
  editora = "Generico";
  titulo = "Generico";
  this->quantidade = quantidade;
  this->codigo = codigo;
  this->preco = preco;
}
Produtos::Produtos(string editora, string titulo, int quantidade, int codigo, float preco){
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
//--------------Destructor------------------------------------------------------
Produtos::~Produtos(){
  cout << "Destruindo o objeto " << endl;
}
//-----------------SET----------------------------------------------------------
void Produtos::set_editora(string editora){
  this->editora = editora;
}
void Produtos::set_titulo(string titulo){
  this->titulo = titulo;
}
void Produtos::set_quantidade(int quantidade){
  this->quantidade = quantidade;
}
void Produtos::set_codigo(int codigo){
  this->codigo = codigo;
}
void Produtos::set_preco(float preco){
  this->preco = preco;
}
//-----------------GET----------------------------------------------------------
string Produtos::get_editora(){
  return editora;
}
string Produtos::get_titulo(){
  return titulo;
}
int Produtos::get_quantidade(){
  return quantidade;
}
int Produtos::get_codigo(){
  return codigo;
}
float Produtos::get_preco(){
  return preco;
}
//methods
