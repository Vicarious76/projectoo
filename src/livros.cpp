#include "livros.hpp"
#include <iostream>

//--------------------------Builders--------------------------------------------
Livros::Livros(){
  autor = "Generico";
  datapublicacao = "Generico";
  assunto = "Generico";
}
Livros::Livros(string editora, string titulo, int quantidade, int codigo, float preco){
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
Livros::Livros(string editora, string titulo, int quantidade, int codigo, float preco, string autor, string datapublicacao, string assunto){
  set_autor(autor);
  set_datapublicacao(datapublicacao);
  set_assunto(assunto);
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
//--------------Destructor------------------------------------------------------
Livros::~Livros(){
  cout << "destrutor da classe" << endl;
}
//-----------------SET----------------------------------------------------------
void Livros::set_autor(string autor){
  this->autor = autor;
}
void Livros::set_datapublicacao(string datapublicacao){
  this->datapublicacao = datapublicacao;
}
void Livros::set_assunto(string assunto){
  this->assunto = assunto;
}
//-----------------GET----------------------------------------------------------
string Livros::get_autor(){
  return autor;
}
string Livros::get_datapublicacao(){
  return datapublicacao;
}
string Livros::get_assunto(){
  return assunto;
}
