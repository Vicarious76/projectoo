#include "gibis.hpp"
#include <iostream>

//--------------------------Builders--------------------------------------------
Gibis::Gibis(){
  tipo = "Generico";
  edicao = "Generico";
  volume = 0;
}
Gibis::Gibis(string editora, string titulo, int quantidade, int codigo, float preco){
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
Gibis::Gibis(string editora, string titulo, int quantidade, int codigo, float preco, string tipo, string edicao, int volume){
  set_tipo(tipo);
  set_edicao(edicao);
  set_volume(volume);
  set_editora(editora);
  set_titulo(titulo);
  set_quantidade(quantidade);
  set_codigo(codigo);
  set_preco(preco);
}
//--------------Destructor------------------------------------------------------
Gibis::~Gibis(){
  cout << "destrutor da classe" << endl;
}
//-----------------SET----------------------------------------------------------
void Gibis::set_tipo(string tipo){
  this->tipo = tipo;
}
void Gibis::set_edicao(string edicao){
  this->tipo = edicao;
}
void Gibis::set_volume(int volume){
  this->volume = volume;
}
//-----------------GET----------------------------------------------------------
string Gibis::get_tipo(){
  return tipo;
}
string Gibis::get_edicao(){
  return edicao;
}
int Gibis::get_volume(){
  return volume;
}
//methods
